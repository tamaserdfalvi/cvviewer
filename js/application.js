var fs = require("fs");
var server = require("./server.js");
var logger = require("./logger.js");
var config = require('../config.js');

var App = {
  logger:logger,
  fs:fs,
  server:server,
  start:function(indexFile,serverPort,serverAddress){
    config.pageTemplate = this.fs.readFileSync(indexFile,"utf8",function(error,data){
      if (error == null){
        return data;
      }
    });
    if ( config.pageTemplate == null ){
      this.logger.logError("Index page not found");
    } else {
      server.create();
      server.start(serverPort,serverAddress);
      this.logger.logMessage("Server started and listening on http://"+serverAddress+":"+serverPort);
    }
  }
};

module.exports = App;
