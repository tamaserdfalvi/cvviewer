
function replaceTemplateStrings(inContent,strings){
  var repl = inContent.replace(/{{[a-zA-Z0-9]+}}/gi,function($1){
    var varName = $1.replace(/{/gi,"").replace(/}/gi,"");
    if (strings[varName] != null ) {
      return strings[varName];
    }
    return $1;
  });
  return repl;
}
module.exports = replaceTemplateStrings;
