var chalk = require('chalk');

var logger = {
  logError:function(message){
    console.error(chalk.red(message));
  },
  logMessage:function(message){
    console.log(chalk.green(message));
  }
};
module.exports = logger;
