var http = require("http");
var url = require('url');
var fs = require("fs");
var logger = require('./logger.js');
var config = require('../config.js');
var replaceTemplateStrings = require('./replaceTemplateStrings.js');

var server = {
  serverObj:null,
  fs:fs,
  replaceTemplateStrings:replaceTemplateStrings,
  create:function(port,address){
    this.serverObj = http.createServer(function(req,res){
      var request = url.parse(req.url);
      server.servePage(request.pathname,req,res);
    });
  },
  servePage:function(path,req,res){
    var page = config.pages.find(page => page.url == path);
    if (page != null){

      if (page.url == "/cvpagesanitize") {
        this.sanitize(req,res);
        return;
      }

      var dataOut = this.fs.readFileSync(page.contentFile,{},function(error,data){
        if (error == null){
          return data;
        }
      });
      if(dataOut != null){
        server.displayPage(dataOut,req,res,page)
      }
    } else {
      res.writeHead(404,{'Content-Type':'text/plain'})
      res.end();
    }
  },

  sanitize:function(req,res){
    var ret = {
      errors:{},
      success:true
    };
    var query = url.parse(req.url, true).query;
    if(query.lastName != null && query.lastName.includes("-")) {
      var strSplit = query.lastName.split("-");
      if(strSplit.length >= 2) {
        res.writeHead(302, {
          'Location': '/?error=Last name contains too much hpyhens'
        });
        res.end();
        return;
      }
    }
    var urlParams = this.mapQueryParametersToUrl(query);
    res.writeHead(302, {
      'Location': '/cvpage?'+urlParams
    });
    res.end();
  },
  mapQueryParametersToUrl:function(query){
    var str = Object.keys(query).map(function(key) {
      return key + '=' + query[key];
    }).join('&');
    return str;
  },
  displayPage:function(content,req,res,page){
    var query = url.parse(req.url, true).query;
    if(page.type=="html"){
      var contentStr = content.toString('utf8');
      var replacedContent = this.replaceTemplateStrings(String(content),query)
      res.writeHead(200,{'Content-Type':'text/html'});
      var contentOut = String(config.pageTemplate).replace("{{content}}",replacedContent);
      res.write(contentOut);
    } else if (page.type=="pdf"){
      res.writeHead(200,{'Content-Type':'text/pdf'});
      res.write(content);
    }
    res.end();
  },
  start:function(port,address){
    this.serverObj.listen(port,address);
  }
};
module.exports = server;
