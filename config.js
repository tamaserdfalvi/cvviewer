var config = {
  pages:[
    {
      url:"/",
      contentFile:"html/main.html",
      type:"html"
    },
    {
      url:"/cvpagesanitize",
      contentFile:"html/cv.html",
      type:"html"
    },
    {
      url:"/cvpage",
      contentFile:"html/cv.html",
      type:"html"
    },
    {
      url:"/resources/cv.pdf",
      contentFile:"resources/cv.pdf",
      type:"pdf"
    }
  ],
  serverAddress:"127.0.0.1",
  serverPort:"8080"
};

module.exports = config;
