var Server = require('../js/server.js');
var config = require('../config.js');

var mockRes = {
  writeHead:function(){},
  end:function(){},
  write:function(){}
};

describe("Application", () => {
  beforeAll(() => {
  });
  afterAll(() => {
  });

  it("should set server serverObj on creation",() => {
    Server.create();
    expect(Server.serverObj).not.toBe(null);
  });

  it("should serve 404 page if page not found in config",() => {
    var oldPages = config.pages;
    var res = {
      writeHead:function(code,info){

      },
      end:function(){

      }
    }
    config.pages = [];
    spyOn(res,'writeHead');
    spyOn(res,'end');
    Server.servePage('nottobefound',{},res);
    expect(res.writeHead).toHaveBeenCalledWith(404,{'Content-Type':'text/plain'});
    expect(res.end).toHaveBeenCalled();
    config.pages = oldPages;
  });

  it("should call to displayPage if content is found",() => {
    spyOn(Server,'displayPage');
    spyOn(Server.fs,'readFileSync').and.returnValue('template');
    var oldPages = config.pages;
    config.pages = [
        {
          url:"tobefound",
          contentFile:"html/main.html",
          type:"html"
        }
    ];
    Server.servePage('tobefound',{url:''},mockRes);
    expect(Server.displayPage).toHaveBeenCalled();
    config.pages = oldPages;
  });

  it("should display page as string if type is html",() => {
    var oldConfig = config.pages;
    spyOn(mockRes,'writeHead');
    spyOn(mockRes,'write');
    config.pageTemplate = 'test template';
    config.pages = [
        {
          url:"tobefound",
          contentFile:"html/main.html",
          type:"html"
        }
    ];
    var buffer = new Buffer('test content');
    Server.displayPage(buffer,config.pages[0],mockRes,config.pages[0]);
    expect(mockRes.writeHead).toHaveBeenCalledWith(200,{'Content-Type':'text/html'});
    expect(mockRes.write).toHaveBeenCalledWith('test template');
    config = oldConfig;
  });

  it("should replace template patterns in content if type is html",() => {
    var page = {
      url:"tobefound",
      contentFile:"html/main.html",
      type:"html"
    };
    var buffer = new Buffer('test content');
    spyOn(Server,'replaceTemplateStrings');
    Server.displayPage(buffer,page,mockRes,page);
    expect(Server.replaceTemplateStrings).toHaveBeenCalled();
  });

  it("should display page as binary if type is pdf",() => {
    var oldConfig = config.pages;
    spyOn(mockRes,'writeHead');
    spyOn(mockRes,'write');
    config.pageTemplate = 'test template';
    config.pages = [
        {
          url:"tobefound",
          contentFile:"html/main.html",
          type:"pdf"
        }
    ];
    var buffer = new Buffer('test content');
    Server.displayPage(buffer,config.pages[0],mockRes,config.pages[0]);
    expect(mockRes.writeHead).toHaveBeenCalledWith(200,{'Content-Type':'text/pdf'});
    expect(mockRes.write).toHaveBeenCalledWith(buffer);
    config = oldConfig;
  });

});
