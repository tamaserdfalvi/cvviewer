var replaceTemplateStrings = require('../js/replaceTemplateStrings.js');

describe("replaceTemplateStrings", () => {
  beforeAll(() => {
  });
  afterAll(() => {
  });

  it("Should replace a variable if found",() => {
    var ret = replaceTemplateStrings("{{var1}}",{var1:"replaced"});
    expect(ret).toBe("replaced");
  });

  it("Should not replace a variable if not found in translations",() => {
    var ret = replaceTemplateStrings("{{var1}}",{});
    expect(ret).toBe("{{var1}}");
  });

});
