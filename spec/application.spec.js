var App = require('../js/application.js');
var config = require('../config.js');


describe("Application", () => {
  beforeAll(() => {
  });
  afterAll(() => {
  });

  it("should log error message if index file is not found",() => {
    spyOn(App.fs,'readFileSync').and.returnValue(null);
    spyOn(App.logger,'logError');
    App.start("nottobefound.html");
    expect(App.logger.logError).toHaveBeenCalled();
  });

  it("should create and start server if indexFile is found",() => {
    spyOn(App.fs,'readFileSync').and.returnValue('template');
    spyOn(App.server,'create');
    spyOn(App.server,'start');
    spyOn(App.logger,'logMessage');
    App.start("Anything");
    expect(App.logger.logMessage).toHaveBeenCalled();
    expect(App.server.start).toHaveBeenCalled();
    expect(App.server.create).toHaveBeenCalled();
  });
});
